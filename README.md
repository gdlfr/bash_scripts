# Bash Scripts

## Docker prune  
  
Use only under **root** priveledges.  
  
### Usage:  

**1. Make executable:**  
```chmod +x <script_name>.sh```

**2. List cronetab jobs:**  
```cronetab -u root -l```  

**3. For edit cronetab jobs:**  
```cronetab -u root -e```   

**4. Add line:**  
```* 3 * * * /root/<script_name>.sh```  
or  
```@hourly /root/<script_name>.sh```  
or  
```@daily /root/<script_name>.sh```  

**5. Check:**  
```cronetab -u root -l```

# Useful commands  

```docker system df```  - docker disk usage, -v for verbose  
```docker version``` - check docker version  
```docker info```  - display system-wide information  
```docker system prune ``` - remove unused data  
```docker network ls -a``` - list docker networks with -a flag to list all  
```docker image ls -aq``` - list docker images in all and quiet mode (show only image id list)  
docker ps = docker container ls  
```docker logs```  - fetch container logs  
```docker inspect```  - return info about Docker objects
