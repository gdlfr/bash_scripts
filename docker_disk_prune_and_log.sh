#!/bin/bash

# This script helps prune docker disk for dangling images, unused containers or images. Use this in a cron tab for
# automatic disk maintenance. While this alone cannot clear most of the fragmented disk which are cleared by restaring
# the daemon, it only delays the point in time where we have to eventually restart the daemon and clear i-nodes
# Usage $ chmod +x docker-disk-prune.sh 
#
# * */3 * * * sh /docker-prune.sh
#
# This script automatically rotates after the threshold is reached. Nothing is required

log=prune.log
log_max_size=5000000 #50 MB

# Do nothing if docker is not installed
if ! hash docker 2>/dev/null; then
    echo "Docker is not installed"
    exit 1
fi

# Create file if it doesn't exist
touch -a $log

# if file size has exceeded the threshold (50 MB), we rotate
current_log_size=$(du ${log} | awk '{print $1}')

if (( current_log_size >= log_max_size ))
    then
        $(echo -n "" > $log)
        echo 'Rotated file ' >> $log
    else
        echo "Current size $current_log_size < $log_max_size (threshold)" >> $log
fi

echo "" >> $log
echo "*********************************" >> $log
date +'=== %Y.%m.%d %H:%M:%S ===' >> $log

#Report current used size
echo "" >> $log
echo "Before " >> $log
docker system df | awk '{print $1 , $4}' >> $log

#Pruning all unused images without prompt
#docker system prune -af >> $log

#Pruning all unused containers without prompt
docker container prune --force

#Report new used size
echo "" >> $log
echo "After " >> $log
docker system df | awk '{print $1 , $4}' >> $log

