#!/bin/bash

#Pruning all unused images without prompt
#docker system prune -af

#Pruning all unused containers without prompt
docker container prune --force
