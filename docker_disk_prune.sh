#!/bin/bash

# This script helps prune docker disk for dangling images, unused containers or images. Use this in a cron tab for
# automatic disk maintenance. While this alone cannot clear most of the fragmented disk which are cleared by restaring
# the daemon, it only delays the point in time where we have to eventually restart the daemon and clear i-nodes
# Usage $ chmod +x docker-disk-prune.sh 
#
# * */3 * * * sh /docker-prune.sh
#
# This script automatically rotates after the threshold is reached. Nothing is required
docker system df | awk '{print $1 , $4}'

#Pruning all unused images without prompt
#docker system prune -af

#Pruning all unused containers without prompt
docker container prune --force

#Report new used size
docker system df | awk '{print $1 , $4}'
