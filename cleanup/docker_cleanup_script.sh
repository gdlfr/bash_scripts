#!/bin/bash

# Docker CleanUp_Script
# v.0.2.1

## list all exited containers
##docker ps -a -f status=exited
##
##sleep 3
##
# remove all exited containers
docker ps -aqf status=exited | xargs -r docker rm
##docker rm $(docker ps -a -f status=exited -q)

sleep 1

docker images ls -a
# To remove all images which are not used by existing containers:
##yes | docker image prune -a
docker images -qf dangling=true | xargs -r docker rmi
##sleep 3

# List dangling volumes
##docker volume ls -f dangling=true
# Remove dangling volumes
##docker volume rm $(docker volume ls -f dangling=true -q)

docker network ls -a

sleep 1

docker rm $(docker ps -qf 'status=exited')
docker rmi $(docker images -qf 'dangling=true')
docker volume rm $(docker volume ls -qf 'dangling=true')


# Removing container logs:
find /var/lib/docker/containers/ -type f -name "*.log" -delete

echo "🧹 Docker cleanup finished!"
