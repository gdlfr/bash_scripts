#!/bin/bash
#LOCAL MACHINE TEST

#fetch log space
echo "You container logs space usage is:"
sudo sh -c "du -ch /var/lib/docker/containers/*/*-json.log"

#logging log space 
sudo sh -c "du -ch /var/lib/docker/containers/*/*-json.log" 1>> ~/docker_logs.log
echo "Cleared:" 1>> ~/docker_logs.log && date +'%Y%m%d%H%M%S' 1>> ~/docker_logs.log

echo "Clean logs? (Y/n)"
read clean

if [[ $clean == "Y" || $clean == "y" ]]; then
	sudo sh -c 'truncate -s 0 /var/lib/docker/containers/*/*-json.log'
	echo "Cleared"
else
	echo "Exit"
fi